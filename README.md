# SINERGIA


![](https://sinergia.pe/assets/images/sinergia-og-01.jpg)

[![Continuous Integration](https://github.com/composer/composer/workflows/Continuous%20Integration/badge.svg?branch=main)](https://github.com/composer/composer/actions)

## Getting Started

### Prerrequisites

PHP (acrónimo recursivo de PHP: Hypertext Preprocessor) es un lenguaje de código abierto muy popular especialmente adecuado para el desarrollo web y que puede ser incrustado en HTML.

Mysql es un sistema de administración relacional de bases de datos.

MariaDb es un sistema de gestión de bases de datos relacionales de código abierto.


### Installation
_Composer install_

Se requiere instalar base de datos Relacionales
[Mysql](https://dev.mysql.com/doc/connector-j/8.1/en/connector-j-reference-jdbc-url-format.html)
 
Se requiere instalar el apache friends para ejecutar PHP
[XAMP](https://www.apachefriends.org/download.html) 

Se requiere instalar la base de datos mariadb
[MariaDb](https://mariadb.org/)

## Api Rest

### Request
 #### Get

curl:https://demo.sinergia.pe/#/home/Configuracion/usuarios

![](https://lh3.googleusercontent.com/7hRxRUuMp0gWyjbvLUDirKNhAQq0_Uzh7tCynbrXxe-lmEchsSo_K_Cueb4cjdraBYHjni6EDjfAE0xC9AmSMPpHmfzpadRDYODF4RV2)

#### Headers

-Server: Apache/2.4.18 (Ubuntu)
-Link: https://162.243.232.68
-ETag: 10ec-5feb9c6d2d117-gzip
-Acept-Ranges: bytes
-User-Agent: PostmanRuntime/7.32.3

[]
#### Others Headers
curl:
Content Length: 1624
Content-Encoding: gzip
vary: Accept-Encoding
Content-Type:HTML/XML
Last Modified: Thu, 22 Jun 2023 15:45:41 

#### Body
   myWorker.addEventListener('message', function (event)

      if (event.data == 200) {
        EventoActivo("activo");
      }
      else if (event.data == 504) {
        EventoError("error");

      }
      else if (event.data == 400) {
        EventoError("noactivo");

        // Estados de Codigo de Configuracion de Maestros
  
## Ussage
Se integrará el api consulta ruc/dni para la empresa Btime en el sistema de sinergia en Registro de (Ventas/Comprobantes). (Añadir). Registro de clientes, Registro de proveedores.

## Contribuiting
Mejorar el codigo para proximo configurar las ventas, clientes y maestros.


  ## Authors and acknowledgment
-Oscar Sauñe Machuca
-Jose Morales Sulca
-Luis Copara


## License
Este proyecto está licenciado bajo la Licencia MIT - consulta el archivo [LICENSE](LICENSE) para más detalles.

## Mainteners

-Jose Morales Sulca

## wikis
[SINERGIA](https://gitlab.com/jorgecedeno28/sinergia/-/wikis/home?view=create)

## Project status
Completado
